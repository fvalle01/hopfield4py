diluted hopfield
================
Run the diluted version of the model

.. automodule:: hopfield4py
    :noindex:
.. autoclass:: diluted_Hopfield
    :members:
    :private-members:
    :inherited-members: