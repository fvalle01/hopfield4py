helper functions
==================
.. automodule:: hopfield4py
    :noindex:
.. autofunction:: hamming
.. autofunction:: get_prediction
.. autofunction:: get_predicted_labels
.. autofunction:: get_all_prediction