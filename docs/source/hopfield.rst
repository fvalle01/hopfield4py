hopfield
=============
.. automodule:: hopfield4py
.. autoclass:: Hopfield
    :members:
    :private-members:
    :inherited-members: