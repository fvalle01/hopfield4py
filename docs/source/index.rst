.. hopfield4py documentation master file, created by
   sphinx-quickstart on Wed Feb  3 10:33:48 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hopfield4py's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. toctree::
   :maxdepth: 2

   hopfield
   diluted_hopfield
   helper_functions


