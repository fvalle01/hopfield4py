from hopfield4py.hopfield import Hopfield
from hopfield4py.diluted import diluted_Hopfield
from hopfield4py.hopfield_helper import *

__version__="0.1.0"